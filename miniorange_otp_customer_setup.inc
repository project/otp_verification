<?php
/**
 * @file
 * Contains form for customer setup.
 */

/**
 * Customer setup form().
 */
function miniorange_otp_customer_setup($form, &$form_state)
{
  global $base_url;

  $current_status = variable_get('miniorange_otp_status', '');

  if ($current_status == 'VALIDATE_OTP') {

    $form['header_top_style_2'] = array(
      '#markup' => '<div class="mo_saml_table_layout_1"><div class="mo_saml_table_layout mo_otp_container">'
    );

    $form['miniorange_otp_customer_otp_token'] = array(
      '#type' => 'textfield',
      '#title' => t('OTP'),
    );

    $form['miniorange_otp_customer_validate_otp_button'] = array(
      '#type' => 'submit',
      '#value' => t('Validate OTP'),
      '#submit' => array('miniorange_otp_validate_otp_submit'),
    );

    $form['miniorange_otp_customer_setup_resendotp'] = array(
      '#type' => 'submit',
      '#value' => t('Resend OTP'),
      '#submit' => array('miniorange_otp_resend_otp'),
    );

    $form['miniorange_otp_customer_setup_back'] = array(
      '#type' => 'submit',
      '#value' => t('Back'),
      '#submit' => array('miniorange_otp_back'),
      '#suffix' => '</div>'
    );

    MiniorangeOtpUtilities::add_support_form_otp($form, $form_state);

    return $form;
  } elseif ($current_status == 'PLUGIN_CONFIGURATION') {
    // Show customer configuration here.
    $form['markup_top'] = array(
      '#markup' => '<div class="mo_saml_table_layout_1"><div class="mo_saml_table_layout mo_otp_container">
                    <div style="display:block;margin-top:10px;text-align: center;font-size: 15px;color:rgba(0, 128, 0, 0.80);background-color:rgba(0, 255, 0, 0.15);padding:5px;">
                         Thank you for registering with miniOrange</div></br>' . '<h4><b>Your Profile:</b></h4>',
    );

    $header = array(
      array('data' => t('Attribute Name')),
      array('data' => t('Attribute Value')),
    );

    $options = array(
      array('Customer Email', variable_get('miniorange_otp_customer_admin_email','')),
      array('Customer ID', variable_get('miniorange_otp_customer_id', '')),
      array('Drupal Version', VERSION),
      array('PHP Version', PHP_VERSION),
    );

    $form['fieldset']['customerinfo'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
    );

    $form['markup_1'] = array(
      '#markup' => '<br><b>Track your Transactions:</b><br>'
        . 'Follow the steps to view your transactions: <br><br>'
        . '1. &nbsp Click on the button below.<br>'
        . '2. &nbsp Login using the credentials you used to register for this plugin.<br>'
        . '3. &nbsp You will be presented with <b><i>View Transactions</i></b> page.<br>'
        . '4. &nbsp From this page you can track your remaining transactions<br><br>',
    );
    $admin_email = variable_get('miniorange_otp_customer_admin_email', NULL);
    $form['markup_2'] = array(
        '#markup' => '<a class="mo_otp_btn mo_otp_btn-primary btn-large" target="_blank" style="padding:5px; text-align:center;" href="https://login.xecurify.com/moas/login?username=' . $admin_email . '&redirectUrl=' . MiniorangeOtpConstants::$baseUrl . '/moas/viewtransactions">View Transaction</a></h4></div>',
    );

    MiniorangeOtpUtilities::add_support_form_otp($form, $form_state);

    return $form;
  }

  $form['markup_14'] = array(
    '#markup' => '<div class="mo_saml_table_layout_1"><div class="mo_saml_table_layout mo_otp_container">
        <h3>Register/Login with miniOrange<br/><br/><hr></h3>',
  );

  $form['markup_15'] = array(
    '#markup' => '<div class="mo_saml_highlight_background_note_1">You can register an account with miniOrange using <a href="https://www.miniorange.com/businessfreetrial" target="_blank">this link</a>. You will also need a miniOrange account to upgrade gateway plans of the module.</div><br>',
  );

  $form['miniorange_otp_customer_setup_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#required' => TRUE,
    '#attributes' => array(
      'placeholder' => 'Enter the email address',
      'required' => ''
    ),
  );

  $form['miniorange_otp_customer_setup_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => TRUE,
    '#attributes' => array(
      'placeholder' => 'Enter the password',
      'required' => ''
    ),
  );

  $form['miniorange_otp_customer_setup_button'] = array(
    '#type' => 'submit',
    '#value' => t('Log in'),
    '#submit' => array('miniorange_otp_customer_setup_submit'),
  );

  $form['miniorange_otp_customer_create_account'] = array(
    '#markup' => '<a class="button button--primary" href="https://www.miniorange.com/businessfreetrial" target="_blank">Create an account</a>',
    '#suffix' => '</div>'
  );

  MiniorangeOtpUtilities::add_support_form_otp($form, $form_state);

  return $form;
}

/**
 * Handle submit for customer setup.
 */
function miniorange_otp_customer_setup_submit(&$form, $form_state)
{

  global $user;
  $user = user_load($user->uid);
  $username = trim($form['miniorange_otp_customer_setup_username']['#value']);
  $phone = '';
  $password = $form['miniorange_otp_customer_setup_password']['#value'] ;

  if (!valid_email_address($username)) {
    drupal_set_message(t('The email address <strong>' . $username . '</strong> is not valid.'), 'error');
    return;
  }

  $customer_config = new MiniorangeOtpCustomer($username, $phone, $password, NULL);
  $check_customer_response = $customer_config->checkCustomer();

  if ($check_customer_response->status == 'CUSTOMER_NOT_FOUND') {
    drupal_set_message(t('Invalid credentials'), 'error');
  } elseif ($check_customer_response->status == 'CURL_ERROR') {
    drupal_set_message(t('cURL is not enabled. Please enable cURL'), 'error');
    return;
  } elseif ($check_customer_response->status == 'SUCCESS') {
    // Customer exists. Retrieve keys.
    $customer_keys_response = $customer_config->getCustomerKeys();
    if (json_last_error() == JSON_ERROR_NONE) {
      variable_set('miniorange_otp_customer_id', $customer_keys_response->id);
      variable_set('miniorange_otp_customer_admin_token', $customer_keys_response->token);
      variable_set('miniorange_otp_customer_admin_email', $username);
      variable_set('miniorange_otp_customer_admin_phone', $phone);
      variable_set('miniorange_otp_customer_api_key', $customer_keys_response->apiKey);
      variable_set('miniorange_otp_status', 'PLUGIN_CONFIGURATION');
      user_save($user);
      drupal_set_message(t('Successfully retrieved your account.'));
      return;
    } else {
      drupal_set_message(t('Invalid credentials'), 'error');
      return;
    }
  } else {
    drupal_set_message(t('An error occured while processing your request. Please try after some time or contact us at <a href="mailto:drupalsupport@xecurify.com">drupalsupport@xecurify.com</a>.'), 'error');
  }
}

/**
 * Handle back button submit for customer setup.
 */
function miniorange_otp_back(&$form, $form_state)
{
  variable_set('miniorange_otp_status', 'CUSTOMER_SETUP');
  variable_del('miniorange_otp_customer_admin_email');
  variable_del('miniorange_otp_customer_admin_phone');
  variable_del('miniorange_otp_tx_id');
  drupal_set_message(t('Register/Login with your miniOrange account.'));
}
